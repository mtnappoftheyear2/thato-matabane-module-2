class App{
  String appName = "shyft";
  String appCategory = "Best Financial Solution";
  String developer = "Standard Bank";
  String year = "2017";


  void appInfo(){
    appNameUpperCase();
    print(appName+","+appCategory+","+developer+","+year);
  }

  void appNameUpperCase(){
    this.appName = appName.toUpperCase();
  }
}

void main(){
  App app = new App();

  app.appInfo();
}